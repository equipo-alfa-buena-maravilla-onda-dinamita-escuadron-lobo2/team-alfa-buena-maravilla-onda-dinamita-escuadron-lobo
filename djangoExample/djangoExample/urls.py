"""
URL configuration for djangoExample project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from core import views
from book import views as book_views
from book.views import  BibliotecaListView, LibroCreate, LibroUpdate, LibroDelete, PrestamoListView, PrestamoCreate

urlpatterns = [
    path('',views.home),
    path('acercade', views.acercade),
    path('home', views.home),
    path("create/", LibroCreate.as_view(), name="create"),
    path("update/<int:pk>/", LibroUpdate.as_view(), name="update"),
    path("delete/<int:pk>/", LibroDelete.as_view(), name="delete"),
    path('contacto', views.contacto),
    path('biblioteca', BibliotecaListView.as_view(), name="libros"),
    path('prestamos', PrestamoListView.as_view(), name='prestamos'),
    path('solicitar/', PrestamoCreate.as_view(), name='solicitar'),
    path('admin/', admin.site.urls),
]

from django.conf import settings

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)