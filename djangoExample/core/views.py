from django.shortcuts import render

# Create your views here.
from django.shortcuts import HttpResponse,render

def home(request):
    return render(request,"core/home.html")

def acercade(request):
    return render(request,"core/acercade.html")

def contacto(request):
    return render(request,"core/contacto.html")
