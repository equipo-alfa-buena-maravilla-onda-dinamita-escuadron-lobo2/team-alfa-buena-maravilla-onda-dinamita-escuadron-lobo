from django.shortcuts import render
from .models import Libro, Prestamo
from django.db.models import Q
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse, reverse_lazy
from django.views.generic.list import ListView
from book.forms import LibroForm, PrestamoForm
# Create your views here.

class BibliotecaListView(ListView):
    model = Libro

    def get_queryset(self):
        query = self.request.GET.get('search')
        if query:
            return Libro.objects.filter(
                Q(title__icontains = query)| 
                Q(autor = query)).distinct()
        else:
            return Libro.objects.all()
        
class LibroCreate(CreateView):
    model = Libro
    success_url = reverse_lazy("libros")
    form_class = LibroForm

class LibroUpdate(UpdateView):
    model = Libro
    form_class = LibroForm
    template_name_suffix = "_update_form"

    def get_success_url(self):
        return reverse_lazy('libros')+'?Actualizado'
    
class LibroDelete(DeleteView):
    model = Libro
    def get_success_url(self):
        return reverse_lazy('libros')+'?Eliminado'

class PrestamoListView(ListView):
    model=Prestamo
    def get_queryset(self):
        query=self.request.GET.get('search')
        if query:
            return Prestamo.objects.filter(
                libro__title__icontains=query
            ).distinct()
        else:
            return Prestamo.objects.all()

class PrestamoCreate(CreateView):
    model=Prestamo
    success_url = reverse_lazy("prestamos")
    form_class = PrestamoForm