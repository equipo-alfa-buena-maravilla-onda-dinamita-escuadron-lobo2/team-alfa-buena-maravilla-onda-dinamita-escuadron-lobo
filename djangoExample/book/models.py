from django.db import models

# Create your models here.

class Libro(models.Model):
    title = models.CharField(max_length=200, verbose_name="Titulo")
    description = models.TextField(verbose_name="Descripcion")
    ide = models.TextField(verbose_name="ID")
    autor = models.TextField(verbose_name="Autor")
    isbn = models.TextField(verbose_name="ISBN")
    number = models.TextField(verbose_name="Numero de inventario")
    estado = models.TextField(verbose_name="Estado")
    image = models.ImageField(verbose_name="Imagen", upload_to="libros")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "libro"
        verbose_name_plural = "libros"
        ordering = ["-created"]

    def __str__(self):
        return self.title

class Prestamo(models.Model):
    libro = models.ForeignKey('Libro', on_delete=models.CASCADE)
    fechaPrestamo = models.DateField()
    fechaDevolucion = models.DateField()
    cliente=models.CharField(max_length=255, verbose_name="Cliente")
    dniCliente=models.CharField(max_length=8, verbose_name="DNI")

    class Meta:
        verbose_name = "prestamo"
        verbose_name_plural = "prestamos"
        ordering = ["-fechaPrestamo"]

    def __str__(self):
        return f"Prestamo de {self.libro} realizado el: {self.fechaPrestamo}. Se debe devolver {self.fechaDevolucion}"
    