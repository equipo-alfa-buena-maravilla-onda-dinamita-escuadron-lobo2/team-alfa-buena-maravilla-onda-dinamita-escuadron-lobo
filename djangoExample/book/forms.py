from django import forms

from .models import Libro, Prestamo


class LibroForm(forms.ModelForm):

    class Meta:
        model = Libro
        fields = ["title","description","ide","autor","isbn","number","estado","image"]
        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Título'}),
            'description': forms.Textarea(attrs={'class':'form-control'}),
            'ide': forms.Textarea(attrs={'class':'form-control'}),
            'autor': forms.Textarea(attrs={'class':'form-control'}),
            'isbn': forms.Textarea(attrs={'class':'form-control'}),
            'number': forms.Textarea(attrs={'class':'form-control'}),
            'estado': forms.Textarea(attrs={'class':'form-control'}),
        }
        labels = {
            'image':''
        }

class PrestamoForm(forms.ModelForm):
    class Meta:
        model = Prestamo
        fields=['libro','fechaPrestamo','fechaDevolucion','cliente','dniCliente']
        widgets={
            'libro':forms.Select(attrs={'class':'form-control'}),
            'fechaPrestamo':forms.DateInput(format=('%Y-%m-%d'),attrs={'class': 'form-control','placeholder': 'Select a date','type': 'date'}),
            'fechaDevolucion':forms.DateInput(format=('%Y-%m-%d'),attrs={'class': 'form-control','placeholder': 'Select a date','type': 'date'}),
            'cliente':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre/s y Apellidos/s'}),
            'dniCliente':forms.TextInput(attrs={'class':'form-control', 'placeholder':'99999999'})
        }
